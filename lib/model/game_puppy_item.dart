class GamePuppyItem {
  String rating;
  String puppyName;
  String imageName;
  double crumbsPercent;
  double bigPercent;

  GamePuppyItem(this.rating, this.puppyName, this.imageName, this.crumbsPercent, this.bigPercent);
}