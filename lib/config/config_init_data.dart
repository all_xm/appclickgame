import 'package:app_click_game/model/game_puppy_item.dart';

//초기값 기재
final List<GamePuppyItem> configPuppies = [
  GamePuppyItem('NORMAL', '목욕하는 강아지', 'dog1.jpg', 11, 0),
  GamePuppyItem('NORMAL', '잔소리하는 강아지', 'dog2.jpg', 11, 0),
  GamePuppyItem('NORMAL', '찌그러진 강아지', 'dog3.jpg', 11, 0),
  GamePuppyItem('NORMAL', '휘핑크림을 얹은 강아지', 'dog4.jpg', 11, 0),
  GamePuppyItem('NORMAL', '곰돌이', 'dog5.jpg', 11, 0),
  GamePuppyItem('NORMAL', '댕삐진 강아지', 'dog6.jpg', 11, 0),
  GamePuppyItem('NORMAL', '선글라스 강아지', 'dog7.jpg', 11, 0),
  GamePuppyItem('NORMAL', '대학원생 강아지', 'dog8.jpg', 11, 0),
  GamePuppyItem('RARE', '입맛을 다시는 강아지', 'dog9.jpg', 1.5, 10),
  GamePuppyItem('RARE', '농부 강아지', 'dog10.jpg', 1.5, 10),
  GamePuppyItem('RARE', '콧물 흘리는 감자', 'dog11.jpg', 1.5, 10),
  GamePuppyItem('RARE', '떼어먹힌 강아지', 'dog12.jpg', 1.5, 10),
  GamePuppyItem('RARE', '바닥에 흘린 강아지', 'dog13.jpg', 1.5, 10),
  GamePuppyItem('UNIQUE', '케르베로스', 'dog14.jpg', 1, 8),
  GamePuppyItem('UNIQUE', '자니…? 강아지', 'dog15.jpg', 1, 8),
  GamePuppyItem('UNIQUE', '강아지? 아니 이건... 토끼', 'dog16.jpg', 1, 8),
  GamePuppyItem('UNIQUE', '유학생 강아지', 'dog17.jpg', 1, 8),
  GamePuppyItem('EPIC', '산신령 강아지', 'dog18.jpg', 0.25, 6.5),
  GamePuppyItem('EPIC', '물개도 강아지야', 'dog19.jpg', 0.24, 6.5),
  GamePuppyItem('LEGENDARY', '파도 강아지', 'dog20.jpg', 0.01, 5),
];
