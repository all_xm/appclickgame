import 'dart:ui';

import 'package:flutter/material.dart';

class ComponentPuppyItem extends StatelessWidget {
  const ComponentPuppyItem({super.key, required this.rating, required this.puppyName, required this.imageName, required this.puppyCount});

  final String rating;
  final String puppyName;
  final String imageName;
  final int puppyCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.all(0.3),
      decoration: BoxDecoration(
        border: Border.all(),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 150,
            height: 150,
            child: _getImage(),
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  puppyCount == 0 ? const Text('-') : _getRatingBadge(),
                  const SizedBox(
                    width: 2,
                  ),
                  Text(puppyCount == 0 ? '-' : puppyName),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              Text('지금까지 만난 횟수 : ${puppyCount}'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _getRatingBadge() {
    switch (rating) {
      case 'RARE':
        return Container(
          width: 45,
          height: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
            color: Colors.greenAccent,
          ),
          child: const Center(
            child: Text(
              '레어',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      case 'UNIQUE':
        return Container(
          width: 45,
          height: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
            color: Colors.indigoAccent,
          ),
          child: const Center(
            child: Text(
              '유니크',
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ),
        );
      case 'EPIC':
        return Container(
          width: 45,
          height: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
            color: Colors.deepOrangeAccent,
          ),
          child: const Center(
            child: Text(
              '에픽',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      case 'LEGENDARY':
        return Container(
          width: 45,
          height: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
            color: Colors.pinkAccent,
          ),
          child: const Center(
            child: Text(
              '레전더리',
              style: TextStyle(color: Colors.white, fontSize: 10),
            ),
          ),
        );
      default:
        return Container(
          width: 45,
          height: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(15),
            color: Colors.amberAccent,
          ),
          child: const Center(
            child: Text(
              '노말',
              style: TextStyle(color: Colors.black45),
            ),
          ),
        );
    }
  }

  Widget _getImage() {
    if (puppyCount == 0) {
      return ImageFiltered(
        imageFilter: ImageFilter.blur(sigmaX: 90, sigmaY: 0),
        child: Image.asset(
          'assets/${imageName}',
          fit: BoxFit.cover,
        ),
      );
    } else {
      return Image.asset(
        'assets/${imageName}',
        width: 150,
        height: 150,
        fit: BoxFit.cover,
      );
    }
  }
}
