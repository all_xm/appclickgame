import 'package:app_click_game/config/config_init_data.dart';
import 'package:app_click_game/model/game_puppy_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageSetting extends StatelessWidget {
  const PageSetting({Key? key}) : super(key: key);

  void _resetMemory() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    prefs.setInt('money', 0);

    List<String> result = [];

    for(GamePuppyItem item in configPuppies) {
      result.add('0');
    }

    prefs.setStringList('usePuppies', result);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ElevatedButton(
          onPressed: () {
            _asyncConfirmDialog(context);
          },
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
          child: Text('게임 리셋'),
        ),
      ),
    );
  }

  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('게임 리셋'),
          content: Text('보유한 돈과 강아지 모두 초기화됩니다.'),
          actions: <Widget>[
            TextButton(
              child: Text('취소'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('확인'),
              onPressed: () {
                _resetMemory();
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
}

