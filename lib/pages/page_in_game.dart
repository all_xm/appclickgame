import 'dart:math';

import 'package:app_click_game/config/config_init_data.dart';
import 'package:app_click_game/model/choose_puppy_item.dart';
import 'package:app_click_game/model/game_puppy_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageInGame extends StatefulWidget {
  const PageInGame({Key? key}) : super(key: key);

  @override
  State<PageInGame> createState() => _PageInGameState();
}

class _PageInGameState extends State<PageInGame> {
  int _money = 0;
  List<String> _usePuppies = [];

  @override
  void initState() {
    super.initState();
    _initMoney();
    _getUsePuppies();
  }

  // 보유한 돈 가져오기. 첫실행일 경우 0원
  void _initMoney() async {
    final prefs = await SharedPreferences.getInstance();
    int oldMoney = prefs.getInt('money') ?? 0;
    setState(() {
      _money = oldMoney;
    });
  }

  // 돈 정보 저장
  void _saveMoney() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('money', _money);
  }

  // 탭했을 때 돈 증가
  void _digUpMoney() {
    setState(() {
      _money += 1000;
    });
  }

  // 보유한 강아지 정보 가져오기
  void _getUsePuppies() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _usePuppies = prefs.getStringList('usePuppies') == null ? [] : prefs.getStringList('usePuppies')!;
    });
  }

  // 강아지 뽑기
  void _getChooseResult(bool isBig) {
    int choosePay = isBig ? 2500 : 500;
    bool isEnoughMoney = _isStartChooseByMoneyCheck(choosePay);

    if (isEnoughMoney) { // 돈이 충분하면
      setState(() {
        _money -= choosePay;
      });

      int puppyResultId = _getChoosePuppy(isBig); // 랜덤으로 강아지 뽑아오기 (index 번호 뽑아옴)

      bool isNewPuppy = false; // 새로 뽑은 유물인지 검사
      int choosePuppyCount = int.parse(
          _usePuppies[puppyResultId]); // 위에서 뽑아온 유물 인덱스 번호에 해당하는 수량 몇개인지 가져옴
      if (choosePuppyCount == 0)
        isNewPuppy = true; // 기존 보유 수량 0이면 새로운 강아지라고 알려줌

      _plusUsePuppy(puppyResultId, choosePuppyCount); // 보유 강아지 수량 1 증가
      _getUsePuppies(); // 보유 강아지 정보 갱신 (원웨이 바인딩이라 새로 알려줘야함)

      _dialogPuppy(isBig, puppyResultId, isNewPuppy);
    }
  }

  // 보유 강아지 정보 세팅
  void _plusUsePuppy(int index, int oldCount) async {
    List<String> usePuppyTemp = _usePuppies; // 보유 강아지 리스트 복사해옴
    _usePuppies[index] =
        (oldCount + 1).toString(); // 수량 증가시킬 강아지에 접근해서 수량 1 증가시킨다

    final prefs = await SharedPreferences.getInstance(); // 메모리 접근
    prefs.setStringList('usePuppies', usePuppyTemp); // 메모리에 덮어쓰기
  }

  // 뽑기 금액이 충분한지 확인
  bool _isStartChooseByMoneyCheck(int choosePay) {
    bool result = false;

    if (_money >= choosePay) result = true;

    return result;
  }

  // 랜덤으로 강아지 뽑기
  int _getChoosePuppy(bool isBig) {
    List<GamePuppyItem> puppies = configPuppies;

    List<ChoosePuppyItem> percentBar = [];

    double oldPercent = 0;
    int index = 0;
    for (GamePuppyItem puppy in puppies) {
      ChoosePuppyItem addItem = ChoosePuppyItem(index, oldPercent, isBig ? oldPercent + puppy.bigPercent : oldPercent + puppy.crumbsPercent);
      percentBar.add(addItem);

      if (isBig) {
        oldPercent += puppy.bigPercent;
      } else {
        oldPercent += puppy.crumbsPercent;
      }

      index++;
    }

    double percentResult = Random().nextDouble() * 100;

    int resultId = 0;
    for (ChoosePuppyItem item in percentBar) {
      if (percentResult >= item.percentMin &&
          percentResult <= item.percentMax) {
        resultId = item.id;
        break;
      }
    }

    return resultId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Stack(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 350,
                child: Image.asset(
                  'assets/ingredient.png',
                  fit: BoxFit.fill,
                ),
              ),
              Positioned(
                top: 300,
                left: MediaQuery.of(context).size.width / 2 - 65,
                child: ElevatedButton(
                  onPressed: _digUpMoney,
                  style: ElevatedButton.styleFrom(
                    primary: Colors.deepOrangeAccent,
                  ),
                  child: const Text("밀가루 모으기"),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 30,
            color: Colors.white,
            child: Center(
              child: Text(
                  '보유한 재료 : ${_money}개',
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2 - 7.5,
                height: 150,
                child: Image.asset('assets/food1.png'),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                children: [
                  SizedBox(
                    child: ElevatedButton(
                      onPressed: () {
                        _getChooseResult(false);
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepOrangeAccent,
                      ),
                      child: const Text('간식 부스러기 만들기'),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 7.5,
                    child: Text('밀가루 500을 소모하여 강아지를 불러낼 수 있습니다.'),
                  ),
                ],
              ),
            ],
          ),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2 - 7.5,
                height: 150,
                child: Image.asset('assets/food2.jpg'),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                children: [
                  SizedBox(
                    child: ElevatedButton(
                      onPressed: () {
                        _getChooseResult(true);
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepOrangeAccent,
                      ),
                      child: const Text('간식 많이 만들기'),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 7.5,
                    child: Text(
                      '밀가루 2,500을 소모하여 강아지를 불러낼 수 있습니다. 희귀한 강아지를 만날 확률이 올라갑니다.',
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> _dialogPuppy(bool isBig, int puppyId, bool isNew) async {
    return showDialog<void>(
      // 다이얼로그 위젯 소환
        context: context,
        barrierDismissible: true, // 다이얼로그 이외의 바탕 누르면 꺼지게 설정
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(isBig ? '간식 많이 만들기 성공!' : '간식 부스러기를 뿌렸더니?'),
            content: SingleChildScrollView(
              child: ListBody(
                // List Body를 기준으로 Text 설정
                children: <Widget>[
                  Image.asset(
                    'assets/${configPuppies[puppyId].imageName}',
                    width: 300,
                    height: 300,
                    fit: BoxFit.cover,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('${configPuppies[puppyId].puppyName}와 마주쳤습니다.'),
                  Text(isNew ? '처음 만난 강아지!' : ' '),
                ],
              ),
            ),
            actions: [
              TextButton(
                child: Text('확인'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
    );
  }

  @override
  void dispose() {
    _saveMoney();
    super.dispose();
  }
}
