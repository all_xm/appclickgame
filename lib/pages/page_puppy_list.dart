import 'package:app_click_game/components/component_puppy_item.dart';
import 'package:app_click_game/config/config_init_data.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PagePuppyList extends StatefulWidget {
  const PagePuppyList({Key? key}) : super(key: key);

  @override
  State<PagePuppyList> createState() => _PagePuppyListState();
}

class _PagePuppyListState extends State<PagePuppyList> {
  List<String> _usePuppies = [];

  void _getUsePuppies() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _usePuppies = prefs.getStringList('usePuppies') == null ? [] : prefs.getStringList('usePuppies')!;
    });
  }

  @override
  void initState() {
    super.initState();

    _getUsePuppies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _usePuppies.length == 0 ? Container() : ListView.builder(
              itemCount: configPuppies.length,
              itemBuilder: (BuildContext ctx, int idx) {
                return ComponentPuppyItem(
                  rating: configPuppies[idx].rating,
                  puppyName: configPuppies[idx].puppyName,
                  imageName: configPuppies[idx].imageName,
                  puppyCount: int.parse(_usePuppies[idx])
                );
              },
            ),
    );
  }
}
