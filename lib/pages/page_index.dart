import 'package:app_click_game/config/config_init_data.dart';
import 'package:app_click_game/model/game_puppy_item.dart';
import 'package:app_click_game/pages/page_in_game.dart';
import 'package:app_click_game/pages/page_puppy_list.dart';
import 'package:app_click_game/pages/page_setting.dart';
import 'package:flutter/material.dart';
import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
} // 디자인적 요소는 이 위에 붙이지 않는다

class _PageIndexState extends State<PageIndex> { // 값을 관리하는 애는 중요하니까 숨겨두기
  int _selectedIndex = 0; // 선택된 인덱스는 기본 0번

  final List<FloatingNavbarItem> _navItems = [
    FloatingNavbarItem(icon: Icons.favorite, title: '수집'),
    FloatingNavbarItem(icon: Icons.pets, title: '강아지 리스트'),
    FloatingNavbarItem(icon: Icons.settings, title: '설정'),
  ]; // 변수가 같이 모여 있으니 한눈에 볼 수 있음

  //0번째 아이템을 선택하면 0번째 테이블을 보여줄 것
  final List<Widget> _widgetPages = [ // 페이지도 위젯을 물려받았으니 위젯. 위젯의 리스트. 자동완성 안 돼도 당황하지 말기!
    PageInGame(), // 0번째 누르면 리스트에서 0번째 꺼내다가 이 페이지를 보여줄 것임
    PagePuppyList(),
    PageSetting(), // 바텀 네비게이션 바 아이콘은 3개여도 여기에는 더 많이 넣어도 됨. 적으면 안 됨! 3번째 클릭했는데 리스트에 없으면 에러남.
  ];

  // _selectedIndex만 바꿀 거니까 리턴 타입 없음
  void _onItemTap(int index) { // 탭을 하면 몇번째인지 알려줌. 그걸 받아옴.
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
void initState() {
    super.initState();

    checkUsePuppies();
}

void checkUsePuppies() async {
  final prefs = await SharedPreferences.getInstance();
  List<String>? checkData = prefs.getStringList('usePuppies');

  bool checkIsEmpty = checkData?.isEmpty ?? true;

  if(checkIsEmpty) {
    List<String> result = [];

    for(GamePuppyItem item in configPuppies) {
      result.add('0');
    }

    prefs.setStringList('usePuppies', result);
  }
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //If you want to show body behind the navbar, it should be true
      extendBody: true,
      body: SafeArea( // 페이지끼리 간섭하면 안 되니 격리
        child: _widgetPages.elementAt(_selectedIndex), // 현재 노출하고 있는 페이지가 보호되어야 함. 현재 보여주는 페이지 = _widgetPages의 _selectedIndex번째 페이지
      ),
      bottomNavigationBar: FloatingNavbar(
        currentIndex: _selectedIndex, // 현재 번호. 선택된 페이지는 흰색으로 표시되어 있으니까 알고 있어야 함
        items: _navItems,
        onTap: _onItemTap, // 바구니 필요없음. onTap 같은 애들은 바구니에 뭘 넣지 않아도 강제 주입한다... 받는 쪽에서 받는다고는 표시해야함
      ),
    );
  }
}
